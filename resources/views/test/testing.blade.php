@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-8">
            <form method="post" action="{{ asset('save_result') }}">
                @csrf
                @foreach($quest as $q)
                <div class="card">
                    <div class="card-header">
                        <span class="text-primary float-right">
                            {{ $q->rasp_name }}
                        </span>
                        {{ $q->name }}
                    </div>
                    <div class="card-body">
                        @foreach($q->projectAnswers as $answer)
                            <label>
                                <input type="radio" name="answer[{{ $q->id }}]" value="{{ $answer->id }}" required>
                                {{ $answer->name }}
                            </label><br>
                        @endforeach
                    </div>
                </div><br />
                @endforeach
                <input type="hidden" name="id_test" value="{{ $id_test }}">
                <input type="submit" id="btn_save" style="display: none">
            </form>
        </div>
        <div class="col-lg-4">
            <label>Имя проекта</label>
            <b>{{ $project->name  }}</b><br>
            <label>Тип проекта</label>
            <b>{{ $type  }}</b><br>
            <hr>
            <label>Кол-во вопросов</label>
            <b>{{ count($quest)  }}</b><br>
            <hr>
            <button class="btn btn-success btn-block" onclick="$('#btn_save').click()">Сохранить результат</button>
        </div>
    </div>
@endsection
