@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-8">
            @foreach($quest as $q)
                <div class="card">
                    <div class="card-header">
                    <span class="text-primary float-right">
                        {{ $q->rasp_name }}
                    </span>
                        {{ $q->name }}
                    </div>
                    <div class="card-body">
                        @foreach($q->projectAnswers as $answer)
                            @if($q->id_type == 1)
                                {{ \App\Helpers\ProjectHelpers::getResultAnswerId($project->id, Auth::id(), $answer->id) }}
                            @else
                                <label>
                                    {{ $answer->name }}
                                </label><br>
                            @endif
                        @endforeach
                    </div>
                </div><br />
            @endforeach
        </div>
        <div class="col-lg-4">
            <b>{{ $user->fio  }}</b><br>
            <hr>
            <label>Имя проекта: </label>
            <b>{{ $project->name  }}</b><br>
            <label>Тип проекта: </label>
            <b>{{ $type  }}</b><br>
            <hr>
            <label>Кол-во вопросов: </label>
            <b>{{ count($quest)  }}</b><br>

            @if(\App\Helpers\UserHelpers::isAdmin() == false)
            <hr>
            <form method="post" action="{{ asset('logout') }}">
                @csrf
                <button type="submit" class="btn btn-danger btn-block">Завершить сессию</button>
            </form>
            @endif
        </div>
    </div>
@endsection
