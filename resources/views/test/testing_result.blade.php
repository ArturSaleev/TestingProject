@extends('layouts.app')

@section('content')
    @php
    $result_true = 0;
    $result_false = 0;
    @endphp

    <div class="row">
        <div class="col-lg-8">
            @foreach($quest as $q)
                <div class="card">
                    <div class="card-header">
                    <span class="text-primary float-right">
                        {{ $q->rasp_name }}
                    </span>
                        {{ $q->name }}
                    </div>
                    <div class="card-body">
                        {{ $res = false }}
                        @foreach($q->projectAnswers as $answer)
                            @isset($user_result[$answer->id])
                                {{ $res = ($res == false) ? (bool) $answer->correct : false }}
                            @endisset
                            <label class="@isset($user_result[$answer->id])
                                @if($res)
                                    text-success
                                @else
                                    text-danger
                                @endif @endisset">
                                {{ $answer->name }}
                            </label><br>
                        @endforeach
                    </div>
                    <div class="card-footer">
                        <label>Результат: {{ ($res) ? 'Положительный' : 'Отрицательный' }}</label>
                        @php
                            if($res == true){
                                $result_true++;
                            } else{
                                $result_false++;
                            }
                        @endphp
                    </div>
                </div><br />
            @endforeach
        </div>
        <div class="col-lg-4">
            <b>{{ $user->fio  }}</b><br>
            <hr>
            <label>Имя проекта: </label>
            <b>{{ $project->name  }}</b><br>
            <label>Тип проекта: </label>
            <b>{{ $type  }}</b><br>
            <hr>
            <label>Кол-во вопросов: </label>
            <b>{{ count($quest)  }}</b><br>
            <label>Правильно отвеченных: </label>
            <b>{{ $result_true  }}</b><br>
            <label>Не правильно отвеченных: </label>
            <b>{{ $result_false  }}</b><br>
            <hr>
            <label>Общий балл: </label>
            <b>{{ round($result_true / count($quest)  * 100, 2)  }}%</b><br>
            @if(\App\Helpers\UserHelpers::isAdmin() == false)
            <hr>
            <form method="post" action="{{ asset('logout') }}">
                @csrf
                <button type="submit" class="btn btn-danger btn-block">Завершить сессию</button>
            </form>
            @endif
        </div>
    </div>
@endsection
