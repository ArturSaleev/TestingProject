@extends('layouts.app')

@section('content')
    @php
        $result_true = 0;
        $result_false = 0;
    @endphp

    <div class="row">
        <div class="col-lg-8">
            @foreach($result as $q)
                <div class="card">
                    <div class="card-header">
                        <span class="text-primary float-right">{{ $q->rasp_name }}</span>
                        {{ $q->name }}
                    </div>
                    <div class="card-body">
                        @foreach($q->list_answer as $answer)
                            {{ $res = '' }}
                            @php
                                if($q->id_answer == $answer->id){
                                    $res = ($q->correct == 1) ? 'text-success' : 'text-danger';
                                }
                            @endphp
                            <label class="{{ $res }}">{{ $answer->name }}</label>
                            <br>
                        @endforeach
                    </div>
                    <div class="card-footer">
                        <label>Результат: {{ ($q->correct == 1) ? 'Положительный' : 'Отрицательный' }}</label>
                    </div>
                </div><br />
            @endforeach
        </div>
        <div class="col-lg-4">
            <b>{{ $user->fio  }}</b><br>
            <hr>
            <label>Имя проекта: </label>
            <b>{{ $project->name  }}</b><br>
            <label>Тип проекта: </label>
            <b>{{ $type  }}</b><br>
            <hr>
            <label>Кол-во вопросов: </label>
            <b>{{ count($result)  }}</b><br>
            <label>Правильно отвеченных: </label>
            <b>{{ $res_true  }}</b><br>
            <label>Не правильно отвеченных: </label>
            <b>{{ $res_false  }}</b><br>
            <hr>
            <label>Общий балл: </label>
            <b>{{ $ball  }}%</b><br>
            @if(\App\Helpers\UserHelpers::isAdmin() == false)
                <hr>
                <form method="post" action="{{ asset('logout') }}">
                    @csrf
                    <button type="submit" class="btn btn-danger btn-block">Завершить сессию</button>
                </form>
            @endif
        </div>
    </div>
@endsection
