@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <center>
                <h1>Проект не найден, закрыт или закончился</h1>
                <form method="post" action="{{ asset('logout') }}">
                    @csrf
                    <button type="submit" class="btn btn-lg btn-danger">
                        Выйти
                    </button>
                </form>
            </center>
        </div>
    </div>
@endsection
