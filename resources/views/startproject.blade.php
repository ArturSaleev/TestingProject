@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <center>
                <h1>Старт проекта</h1>
                <form method="post">
                    @csrf
                    <button type="submit" class="btn btn-lg btn-primary">
                        Начать {{ $type }}
                    </button>
                </form>
            </center>
        </div>
    </div>
@endsection
