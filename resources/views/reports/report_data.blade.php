<div class="card">
    <div class="card-header">
        <h5>Данные по проекту</h5>
    </div>
    <div class="card-body">
        <div class="list-group">
            <table border="0" width="100%">
                <tr>
                    <td>Общее кол-во вопросов</td>
                    <td>{{ $count_quest }}</td>
                </tr>
                <tr>
                    <td>Кол-во отображаемых вопросов</td>
                    <td>{{ ($project->cnt_quest == 0) ? 'Все' : $project->cnt_quest }}</td>
                </tr>
                <tr>
                    <td>Кол-во сдавших людей</td>
                    <td>{{ $count_users }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
</div>
