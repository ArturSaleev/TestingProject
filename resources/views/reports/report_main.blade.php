@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h3>Отчет по проектам</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4" id="step_1">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Выберите проект</h5>
                                </div>
                                <div class="card-body">
                                    <div class="list-group">
                                        @foreach($result as $res)
                                            <span class="list-group-item list-group-item-action list_projects" id="{{ $res->id }}">
                                                {{ $res->name }}<br>
                                                <span class="form-text text-muted">
                                                    c {{ date('d.m.Y', strtotime($res->date_begin)) }} - {{ ($res->date_end == '') ? 'по наст. вр.' :  date('d.m.Y', strtotime($res->date_end)) }}
                                                </span>
                                            </span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-4" id="step_2">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script>
        $('.list_projects').click(function(){
            let id = $(this).attr('id');
            $('.list_projects.active').removeClass('active');
            $(this).addClass('active');
            $.get('{{ asset('report/test') }}/'+id, function(data){
                $('#step_2').html(data);
            })
        });
    </script>
@endsection
