@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    @if($on_back)
                        <a href="{{ asset('report/all_users_test') }}" class="btn btn-primary float-right">Назад</a>
                    @endif
                    <h3>Отчет пользователей и проектов</h3>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTabContent">
                        <li class="nav-item active">
                            <a class="nav-link" data-bs-toggle="tab" href="#tab_1">Прошедшие (частично или полностью)</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="tab" href="#tab_2">Не сдавшие</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    @foreach($tr as $trr)
                                        <th style="font-size: 10px;">
                                            {{ $trr['name'] }}
                                            @isset($trr['type_name'])
                                                <br><span style="font-size: 8px">({{ $trr['type_name'] }})</span>
                                            @endisset
                                        </th>
                                    @endforeach
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($td as $tdd)
                                    <tr>
                                        @foreach($tdd as $tds)
                                            <td>{!! $tds !!}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="tab-pane" id="tab_2">

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ИИН</th>
                                    <th>Фамилия Имя Отчество</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($not_passed as $passed)
                                    <tr>
                                        <td>{{ $passed['id'] }}</td>
                                        <td>{{ $passed['iin'] }}</td>
                                        <td>{{ $passed['name'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#myTabContent a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        })
    </script>
@endsection
