@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Авторизация') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Введите ИИН') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="number" maxlength="12" class="form-control" name="iin" value="{{ old('email') }}" required autofocus>

                                @isset($message)
                                    {{ $message }}
                                @endisset
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Войти
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        $('#email').keyup(function(e){
            let val = $(this).val();
            let t = val.length;
            let r = val.substr(0, 12);
            if(t >= 12){
                $(this).val(r);
            }
        })
    </script>
@endsection
