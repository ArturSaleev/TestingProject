@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <button class="btn btn-primary btn-sm float-right"  data-toggle="modal" data-target="#editProjectModal" onclick="getProjectData(0)">
                        Создать новый проект
                    </button>
                    <h3>Список проектов</h3>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Название проекта</th>
                            <th>Начало</th>
                            <th>Окончание</th>
                            <th>Кол-во вопросов/Отображаемые</th>
                            <th>Ссылка</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $k=>$ds)
                            <tr>
                                <td>{{ $k+1 }}</td>
                                <td><a href="/project/{{ $ds->id  }}">{{ $ds->name }}</a></td>
                                <td>{{ date('d.m.Y', strtotime($ds->date_begin)) }}</td>
                                <td>{{ $ds->date_end }}</td>
                                <td>{{ $ds->project_quests_count }} / {{ ($ds->cnt_quest > 0) ? $ds->cnt_quest : $ds->project_quests_count }}</td>
                                <td>{{ asset('?id='.$ds->uid) }}</td>
                                <td width="120px">
                                    <span class="btn btn-success btn-sm" data-toggle="modal" data-target="#editProjectModal" onclick="getProjectData({{ $ds->id }})"><i class="fa fa-edit"></i></span>
                                    <span class="btn btn-danger btn-sm" onclick="removeProject({{ $ds->id }})"><i class="fa fa-trash"></i></span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="editProjectModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Редактор проекта</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ asset('project') }}">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label>Наименование</label>
                            <input type="text" class="form-control" id="name" name="name" value="" required>
                        </div>
                        <div class="form-group">
                            <label>Тип проекта</label>
                            <select class="form-control" id="id_type" name="id_type">
                                @foreach($type as $tp)
                                    <option value="{{ $tp->id }}">{{ $tp->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Дата начала</label>
                            <input type="date" class="form-control" id="date_begin" name="date_begin" value="" required>
                        </div>
                        <div class="form-group">
                            <label>Дата окончания</label>
                            <input type="date" class="form-control" id="date_end" name="date_end" value="">
                        </div>
                        <div class="form-group">
                            <label>Кол-во отображаемых позиций (Вопросов)</label>
                            <input type="number" class="form-control" id="cnt_quest" name="cnt_quest" value="0">
                            <span class="text-info">Установите кол-во отображаемых вопрос для отображения при сдачи проекта. Если установить 0 то будут отображаться все позиции</span>
                        </div>
                    </div>
                    <input type="hidden" id="id_project" name="id" value="0">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="save">Сохранить</button>
                        <span class="btn btn-secondary" data-dismiss="modal">Закрыть</span>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function getProjectData(id) {
            $('#id_project').val(id);
            $.get('{{ asset('/project/edit') }}/' + id, function (data) {
                let name = '';
                let type = 1;
                let date_begin = '{{ date('Y-m-d') }}';
                let date_end = '';
                let cnt_quest = 0;
                if(id !== 0){
                    name = data.name;
                    type = data.id_type;
                    date_begin = data.date_begin;
                    date_end = data.date_end;
                    cnt_quest = data.cnt_quest;
                }

                $('#name').val(name);
                $('#id_type').val(type);
                $('#date_begin').val(date_begin);
                $('#date_end').val(date_end);
                $('#cnt_quest').val(cnt_quest);
            });
        }

        function removeProject(id)
        {
            console.log(id);
        }
    </script>
@endsection
