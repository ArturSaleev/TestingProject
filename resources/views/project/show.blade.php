@extends('layouts.app')

@section('content')
    @if($data->id_type == 1)
        @include('project.show.test')
    @else
        @include('project.show.quest')
    @endif
@endsection
