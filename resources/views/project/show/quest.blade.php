@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Список вопросов</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($quests as $quest)
                            <tr>
                                <td colspan="2">
                                    <b>{{ $quest->name }}</b> ({{ $quest->rasp_name }})
                                </td>
                                <td width="120px">
                                    <span class="btn btn-success btn-sm" data-toggle="modal" data-target="#editModal" onclick="edit({{ $quest->id }})"><i
                                            class="fa fa-edit"></i></span>
                                    <span class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></span>
                                </td>
                            </tr>
                            @if($quest->id_type == '0')
                            @foreach($quest->projectAnswers as $answer)
                                <tr>
                                    <td style="text-align: right">- {{ $answer->name }}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <label>Название проекта: <b>{{ $data->name }}</b></label><br/>
                    <label>Дата начала: <b>{{ date('d.m.Y', strtotime($data->date_begin)) }}</b></label><br/>
                    <label>Дата окончания: <b>
                            @if($data->date_end !== null)
                                {{  date('d.m.Y', strtotime($data->date_end)) }}
                            @endif
                        </b>
                    </label><br/>
                    <hr/>
                    <span class="btn btn-primary btn-block" data-toggle="modal"
                          data-target="#editModal" onclick="edit(0)">Добавить вопрос</span>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Редактор вопросов</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" id="form_answer_save">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label>Введите вопрос</label>
                            <input type="text" class="form-control" id="name" name="quest_name" value="" required>
                        </div>
                        <div class="form-group">
                            <label>Тема вопроса</label>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <a href="#" class="input-group-text" data-toggle="modal" data-target="#editRasp">
                                        <i class="fa fa-plus-circle"></i>
                                    </a>
                                    <select class="form-control" name="id_rasp" id="id_rasp">
                                        <option value="0">-- не выбран</option>
                                        @foreach($rasp as $r)
                                            <option value="{{ $r->id }}">{{ $r->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <span class="btn btn-dark btn-sm float-right" id="new_answer">
                            <i class="fa fa-plus-circle"></i>
                        </span>
                        <label class="h4">Варианты ответов</label>
                        <label>
                            <input type="checkbox" name="id_type" id="id_type" value="1">
                            Тестовое поле
                        </label>
                        <div id="answer_array">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control answer" name="answer[1]" id="1"
                                           placeholder="Введите вариант ответа" required>
                                    <span class="input-group-text">
                                    <input type="radio" name="answer_true" value="1">
                                </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="id_quest" name="id" value="0">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="save">Сохранить</button>
                        <span class="btn btn-secondary" data-dismiss="modal">Закрыть</span>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Modal rasp -->
    <div class="modal fade" id="editRasp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Редактировать тему</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label>Наименование</label>
                        <input type="text" class="form-control" id="name_rasp" value="" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="save_rasp">Сохранить</button>
                    <span class="btn btn-secondary" data-dismiss="modal">Закрыть</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        let token = "{{ csrf_token() }}";
        function new_answer(id, name='', checked=false)
        {
            let ch = (checked) ? 'checked' : '';
            let html = '<div class="form-group" id="'+id+'">\n' +
                '            <div class="input-group mb-3">\n' +
                '                <span class="input-group-text bg-danger btn btn-danger trash_answer" id="'+id+'">\n' +
                '                    <i class="fa fa-trash"></i>\n' +
                '                </span>\n' +
                '                <input type="text" class="form-control answer" name="answer['+id+']" placeholder="Введите вариант ответа" value="'+name+'">\n' +
                '            </div>\n' +
                '        </div>';
            $('#answer_array').append(html);
        }

        $('#new_answer').click(function(){
            let id = Math.round(Math.random()*1000000);
            new_answer(id);
        });

        $('body').on('click', '.trash_answer', function(){
            let id = $(this).attr('id');
            $('.form-group#'+id).remove();
        });

        $('#form_answer_save').submit(function (e){
            e.preventDefault();
            let form = $(this).serialize();
            $.post(window.location.href, form, function(data){
                if(typeof(data.message) !== "undefined"){
                    alert(data.message);
                    return false;
                }
                location.reload();
            })
        })

        $('#save_rasp').click(function(){
            let name = $('#name_rasp').val();
            let resp = {
                "_token": token,
                "name" : name
            }

            $.post("{{ asset('quest_rasp') }}", resp, function(data){
                if(typeof(data.message) !== "undefined"){
                    alert(data.message);
                }
                let ir = $('#id_rasp');
                ir.html('');
                let id = 0;
                $.each(data.data, function(i, e){
                    ir.append('<option value="'+e.id+'">'+e.name+'</option>')
                    id = e.id;
                })
                $('#name_rasp').val('');
                $('#editRasp').modal().hide();
                $('#id_rasp').val(id);
            })
        });

        function edit(id)
        {
            $('#id_quest').val(id);
            $('input[name=quest_name]').val('');
            $('#id_rasp').val(0);
            $('#answer_array').html('');
            $('.answer#1').val('');
            $('.answer#2').val('');
            $('input[name=answer_true]').prop('checked', false);
            if(id > 0){
                $.get("{{ asset('quests') }}/"+id, function(data){
                    $('input[name=quest_name]').val(data.name);
                    $('#id_rasp').val(data.id_rasp);
                    $.each(data.project_answers, function(i, e){
                        if(i >= 2){
                            let id = Math.round(Math.random()*1000000);
                            new_answer(id, e.name, (e.correct == 1));
                        }else{
                            let ids = i+1;
                            $('.answer#'+ids).val(e.name);
                            $('input[name=answer_true]').each(function(i, e){
                                this.checked = (e.value === ids.toString());
                            })
                        }
                    })
                })
            }
        }
        let html_answer = '';
        $('#id_type').change(function(){
            let b = $(this).prop('checked');
            $('#new_answer').css('display', (b)? 'none':'');
            if(b){
                html_answer = $('#answer_array').html();
                $('#answer_array').html('');
            }else{
                $('#answer_array').html(html_answer);
            }
        })
    </script>
@endsection
