@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <button class="btn btn-primary btn-sm float-right"  data-toggle="modal" data-target="#editUsers" onclick="getUserData(0)">
                        Добавить пользователя
                    </button>
                    <h3>Список пользователей</h3>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ФИО</th>
                            <th>ИИН/ИНН</th>
                            <th>Роль</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->fio }}</td>
                                <td>{{ $user->iin }}</td>
                                <td>{{ ($user->isadmin == 1) ? 'Администратор' : 'Пользователь' }}</td>
                                <td width="120px">
                                    <span class="btn btn-success btn-sm" data-toggle="modal" data-target="#editUsers" onclick="getUserData({{ $user->id }})"><i class="fa fa-edit"></i></span>
                                    <span class="btn btn-danger btn-sm" onclick="removeProject({{ $user->id }})"><i class="fa fa-trash"></i></span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="editUsers" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Редактор данных</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ asset('users') }}">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label>ФИО</label>
                            <input type="text" class="form-control" id="fio" name="fio" value="" required>
                        </div>
                        <div class="form-group">
                            <label>ИИН/ИНН</label>
                            <input type="text" class="form-control" id="iin" name="iin" value="" maxlength="12" required>
                        </div>
                        <div class="form-group">
                            <label>Роль</label>
                            <select class="form-control" id="isadmin" name="isadmin">
                                <option value="0">Пользователь</option>
                                <option value="1">Администратор</option>
                            </select>
                        </div>

                    </div>
                    <input type="hidden" id="id" name="id" value="0">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="save">Сохранить</button>
                        <span class="btn btn-secondary" data-dismiss="modal">Закрыть</span>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    function getUserData(id)
    {
        if(id === 0){
            $('#fio').val("");
            $('#iin').val("");
            $('#isadmin').val('0');
            $('#id').val('0');
            return;
        }

        $.get('{{ asset('users') }}/'+id, function(data){
            $('#fio').val(data.fio);
            $('#iin').val(data.iin);
            $('#isadmin').val(data.isadmin);
            $('#id').val(data.id);
        })
    }
</script>
@endsection
