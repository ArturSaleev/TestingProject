<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\QuestRaspController;
use App\Http\Controllers\QuestsController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', [ProjectController::class, 'index']);
    Route::post('/', [ProjectController::class, 'start']);
    Route::get('/project/edit/{id}', [ProjectController::class, 'edit']);
    Route::get('/project/{id}', [ProjectController::class, 'show']);
    Route::post('/project', [ProjectController::class, 'store']);
    Route::post('/project/{id}', [ProjectController::class, 'save_quest']);

    Route::post('/quest_rasp', [QuestRaspController::class, 'store']);
    Route::get('/quests/{id_project}', [QuestsController::class, 'show']);

    Route::post('save_result', [TestController::class, 'store']);

    Route::get("/report/all_users_test", [ReportController::class, 'all_users_test']);
    Route::get('/report/view_result', [ReportController::class, 'view_result']);
    Route::get('/report/test', [ReportController::class, 'get_main_report']);
    Route::get('/report/test/{id}', [ReportController::class, 'get_data_report']);

    Route::resource('users', UsersController::class);
});
