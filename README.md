<p align="center">
    <h1>Система тестирования и опросников</h1>
</p>

## Описание

Система для создания опросников и тестирвоания сотрудников или определенной группы людей

## Установка
- cp .env.example .env

Set configuration connect to database for **.env** file

Настройка подключения к определенной базе данных

- php artisan key:generate
- php artisan migrate
- php artisan db:seed
- php artisan serve

## Базы данных

Для использования данной системы подходят базы данных:
- MySQL
- PostgreSQL
- SQLite
- 

Для использования базы данных Oracle необходимо установить компонент **yajra/laravel-oci8**
- https://yajrabox.com/docs/laravel-oci8

or
- composer require yajra/laravel-oci8
