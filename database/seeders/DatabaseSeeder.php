<?php

namespace Database\Seeders;

use App\Models\RefType;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "iin" => "000000000000",
            "fio" => "Администратор",
            "isadmin" => 1
        ];

        User::query()->insert($data);
        User::factory(10)->create();

        $ref_type_data = [
            ["id" => 1, "name" => "Тестирование"],
            ["id" => 2, "name" => "Опросник"]
        ];
        RefType::query()->insert($ref_type_data);
    }
}
