<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToUsersTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_test', function (Blueprint $table) {
            $table->foreign('id_project', 'FK_ut_p')->references('id')->on('projects');
            $table->foreign('id_user', 'FK_ut_u')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_test', function (Blueprint $table) {
            $table->dropForeign('FK_ut_p');
            $table->dropForeign('FK_ut_u');
        });
    }
}
