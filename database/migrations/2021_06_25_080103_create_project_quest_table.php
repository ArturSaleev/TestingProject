<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectQuestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_quest', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('id_project')->index('FK_pq_p');
            $table->string('name')->comment('Вопрос');
            $table->integer('id_rasp')->nullable()->default(0);
            $table->integer('id_type')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_quest');
    }
}
