<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToUserTestAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_test_answers', function (Blueprint $table) {
            $table->foreign('id_answer', 'FK_uta_pa')->references('id')->on('project_answers');
            $table->foreign('id_user_test', 'FK_uta_ut')->references('id')->on('users_test');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_test_answers', function (Blueprint $table) {
            $table->dropForeign('FK_uta_pa');
            $table->dropForeign('FK_uta_ut');
        });
    }
}
