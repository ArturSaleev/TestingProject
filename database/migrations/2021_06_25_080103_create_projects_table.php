<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('id_type')->default(0)->index('FK_projects_type');
            $table->string('name')->comment('название проекта');
            $table->date('date_begin')->comment('Период действия проекта');
            $table->date('date_end')->nullable()->comment('Период окончания проекта');
            $table->tinyInteger('state')->default(1)->comment('Статус');
            $table->integer('id_user')->index('FK_projects_users');
            $table->string('uid')->nullable();
            $table->integer('cnt_quest')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
