<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_answers', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('id_quest')->index('FK_pa_pq');
            $table->text('name')->comment('Вариант ответа');
            $table->tinyInteger('correct')->default(0)->comment('Правельный вариант ответа');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_answers');
    }
}
