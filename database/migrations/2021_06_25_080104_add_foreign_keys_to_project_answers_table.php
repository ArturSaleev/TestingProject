<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToProjectAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_answers', function (Blueprint $table) {
            $table->foreign('id_quest', 'FK_pa_pq')->references('id')->on('project_quest');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_answers', function (Blueprint $table) {
            $table->dropForeign('FK_pa_pq');
        });
    }
}
