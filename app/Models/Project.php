<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property int $id_type
 * @property int $id_user
 * @property string $name
 * @property string $date_begin
 * @property string $date_end
 * @property boolean $state
 * @property string $uid
 * @property int $cnt_quest
 * @property RefType $refType
 * @property User $user
 * @property ProjectQuest[] $projectQuests
 * @property UsersTest[] $usersTests
 */
class Project extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_type', 'id_user', 'name', 'date_begin', 'date_end', 'state', 'uid', 'cnt_quest'];

    protected $casts = [
        'date_begin' => 'datetime:Y-m-d',
        'date_end' => 'datetime:Y-m-d'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function refType()
    {
        return $this->belongsTo('App\Models\RefType', 'id_type');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projectQuests()
    {
        return $this->hasMany('App\Models\ProjectQuest', 'id_project');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usersTests()
    {
        return $this->hasMany('App\Models\UsersTest', 'id_project');
    }
}
