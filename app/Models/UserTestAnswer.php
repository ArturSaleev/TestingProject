<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_user_test
 * @property int $id_answer
 * @property string $result_text
 * @property ProjectAnswer $projectAnswer
 * @property UsersTest $usersTest
 */
class UserTestAnswer extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_user_test', 'id_answer', 'result_text'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function projectAnswer()
    {
        return $this->belongsTo('App\Models\ProjectAnswer', 'id_answer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usersTest()
    {
        return $this->belongsTo('App\Models\UsersTest', 'id_user_test');
    }
}
