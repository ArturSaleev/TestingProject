<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;

/**
 * @property int $id
 * @property string $fio
 * @property string $dolgnost
 * @property boolean $isadmin
 * @property string $token
 * @property string $iin
 * @property Project[] $projects
 * @property UsersTest[] $usersTests
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable;
    /**
     * @var array
     */
    protected $fillable = ['fio', 'dolgnost', 'isadmin', 'token', 'iin'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects()
    {
        return $this->hasMany('App\Models\Project', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function usersTests()
    {
        return $this->hasMany('App\Models\UsersTest', 'id_user');
    }
}
