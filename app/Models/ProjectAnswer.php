<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_quest
 * @property string $name
 * @property boolean $correct
 * @property ProjectQuest $projectQuest
 * @property UserTestAnswer[] $userTestAnswers
 */
class ProjectAnswer extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['id_quest', 'name', 'correct'];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function projectQuest()
    {
        return $this->belongsTo('App\Models\ProjectQuest', 'id_quest');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userTestAnswers()
    {
        return $this->hasMany('App\Models\UserTestAnswer', 'id_answer');
    }
}
