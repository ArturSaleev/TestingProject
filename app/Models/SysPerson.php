<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $iin
 * @property string $lastname
 * @property string $firstname
 * @property string $middlename
 * @property string $avatar
 * @property string $birthday
 * @property int $sex_id
 * @property int $doc_type
 * @property string $doc_ser
 * @property string $doc_num
 * @property string $doc_issued_org_
 * @property string $doc_date_begin
 * @property string $doc_date_end
 * @property string $phone
 * @property string $mob_phone
 * @property string $email
 * @property string $fact_addres
 * @property int $reg_addr_city
 * @property int $reg_addr_street
 * @property int $reg_addr_build
 * @property int $reg_addr_korpus
 * @property int $reg_addr_flat
 * @property int $fact_addr_city
 * @property int $fact_addr_street
 * @property int $fact_addr_build
 * @property int $fact_addr_korpus
 * @property int $fact_addr_flat
 * @property int $national_cod
 * @property int $citizenship_cod
 * @property int $social_group
 * @property int $insur_state
 * @property string $birth_place
 * @property int $marital_state
 * @property int $work_place_cod
 * @property int $org_of_attach
 * @property int $education_cod
 * @property int $profession_cod
 * @property SysPersonal[] $sysPersonals
 */
class SysPerson extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'sys_person';

    /**
     * @var array
     */
    protected $fillable = ['iin', 'lastname', 'firstname', 'middlename', 'avatar', 'birthday', 'sex_id', 'doc_type', 'doc_ser', 'doc_num', 'doc_issued_org_', 'doc_date_begin', 'doc_date_end', 'phone', 'mob_phone', 'email', 'fact_addres', 'reg_addr_city', 'reg_addr_street', 'reg_addr_build', 'reg_addr_korpus', 'reg_addr_flat', 'fact_addr_city', 'fact_addr_street', 'fact_addr_build', 'fact_addr_korpus', 'fact_addr_flat', 'national_cod', 'citizenship_cod', 'social_group', 'insur_state', 'birth_place', 'marital_state', 'work_place_cod', 'org_of_attach', 'education_cod', 'profession_cod'];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sysPersonals()
    {
        return $this->hasMany('App\Models\SysPersonal', 'person_id');
    }
}
