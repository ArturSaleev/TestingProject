<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_user
 * @property int $id_project
 * @property string $date_begin
 * @property Project $project
 * @property User $user
 * @property UserTestAnswer[] $userTestAnswers
 */
class UsersTest extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'users_test';

    /**
     * @var array
     */
    protected $fillable = ['id_user', 'id_project', 'date_begin'];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'id_project');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userTestAnswers()
    {
        return $this->hasMany('App\Models\UserTestAnswer', 'id_user_test');
    }
}
