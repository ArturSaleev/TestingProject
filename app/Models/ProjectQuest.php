<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_project
 * @property string $name
 * @property int $id_rasp
 * @property int $id_type
 * @property Project $project
 * @property ProjectAnswer[] $projectAnswers
 */
class ProjectQuest extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'project_quest';

    /**
     * @var array
     */
    protected $fillable = ['id_project', 'name', 'id_rasp', 'id_type'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'id_project');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projectAnswers()
    {
        return $this->hasMany('App\Models\ProjectAnswer', 'id_quest');
    }
}
