<?php

namespace App\Http\Controllers;

use App\Models\ProjectQuest;
use Illuminate\Http\Request;

class QuestsController extends Controller
{
    public function show($id)
    {
        $q =  ProjectQuest::with('projectAnswers')
            ->where('id', $id)
            ->first([
                'project_quest.id',
                'project_quest.name',
                'project_quest.id_rasp'
            ]);
        return response()->json($q);
    }
}
