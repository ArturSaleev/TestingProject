<?php

namespace App\Http\Controllers;

use App\Helpers\ProjectHelpers;
use App\Helpers\UserHelpers;
use App\Models\Project;
use App\Models\ProjectQuest;
use App\Models\User;
use App\Models\UsersTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{

    public function all_users_test(Request $request)
    {
        if(!UserHelpers::isAdmin()){
            abort(404);
        }

        $data = [
            "tr" => [
                ["name" => "#", "id" => 0],
                ["name" => "ИИН", "id" => 0],
                ["name" => "Фамилия Имя Отчество", "id" => 0]
            ],
            "td" => []
        ];

        $projects = Project::query()
            ->orderBy('id_type')
            ->get(['id', "name", "id_type"]);

        foreach($projects as $project){
            $rf = $project->refType->name;
            $data['tr'][] =  [
                "id" => $project->id,
                "name" => $project->name,
                "type" => $project->id_type,
                "type_name" => $rf
            ];
        }


        $users = User::query()
            //->limit('10')
            ->get(["id", "iin", "fio"]);

        if($request->has("id_user")){
            $users = User::query()
                ->where("id", $request->id_user)
                ->get(["id", "iin", "fio"]);
        }

        $data['not_passed'] = [];

        foreach($users as $user){
            $ds = [
                $user->id,
                '<a href="/report/all_users_test?id_user='.$user->id.'">'.$user->iin.'</a>',
                $user->fio
            ];
            $i = 0;
            foreach($projects as $project){
                $usersTest = UsersTest::withCount("userTestAnswers")
                    ->where("id_project", $project->id)
                    ->where("id_user", $user->id)
                    ->first();

                try {
                    $cnt = $usersTest->user_test_answers_count;
                    $link = '<a target="_blank" href="/report/view_result?id_user='.$user->id.'&id_project='.$project->id.'">'.$cnt.'</a>';
                    $ds[] = ((int)$cnt == 0) ? "" : $link;
                    if((int)$cnt > 0){
                        $i++;
                    }
                }catch(\Exception $e){
                    $ds[] = "";
                }
            }
            if($i == 0){
                $data['not_passed'][] = [
                    "id" => $user->id,
                    "name" => $user->fio,
                    "iin" => $user->iin,
                ];
            }else {
                $data["td"][] = $ds;
            }
        }

        $data['on_back'] = (count($request->all()) > 0);
        return view('reports.all_users_test', $data);
    }

    public function view_result(Request $request)
    {
        if(!UserHelpers::isAdmin()){
            abort(404);
        }

        if(!$request->has('id_project')){
            abort(404);;
        }
        if(!$request->has('id_user')){
            abort(404);;
        }
        $id_user = $request->id_user;
        $id_project = $request->id_project;

        //-------------------------------------------
        $q = Project::find($id_project);
        $type = $q->refType->name;

        $project = UsersTest::query()
            ->where(['id_user' => $id_user, "id_project" => $id_project])
            ->first();

        $result = ProjectHelpers::getProjectQuestOnResult($id_project, $id_user);

        $result_true = 0;
        $result_false = 0;

        foreach($result as $res){
            if($res->correct == 1){
                $result_true++;
            }else{
                $result_false++;
            }
        }

        $ball = round($result_true / count($result)  * 100, 2);

        $data = [
            "user" => User::find($id_user),
            "id_test" => $project->id,
            "project" => $project->project,
            "type" => $type,
            "result" => $result,
            'res_true' => $result_true,
            'res_false' => $result_false,
            'ball' => $ball
        ];

//        dd($data);
        return view('test.result', $data);

    }

    public function get_main_report()
    {
        $project = Project::all();
        return view('reports.report_main', ["result" => $project]);
    }

    public function get_data_report($id)
    {
        $project = Project::find($id);
        $count_quest = ProjectQuest::query()->where('id_project', $id)->count();
        $count_users = UsersTest::query()->where('id_project', $id)->count();

        return view('reports.report_data', [
            "project" => $project,
            "count_quest" => $count_quest,
            "count_users" => $count_users,
        ]);
    }
}
