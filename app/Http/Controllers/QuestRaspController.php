<?php

namespace App\Http\Controllers;

use App\Models\QuestRasp;
use Illuminate\Http\Request;

class QuestRaspController extends Controller
{
    public function index()
    {
        return response()->json(['data' => QuestRasp::all()]);
    }

    public function store(Request $request)
    {
        $id = ($request->has('id')) ? $request->post('id') : 0;
        $name = $request->post('name');

        $qr = ($id == 0) ? new QuestRasp() : QuestRasp::find($id);
        $qr->name = $name;
        try {
            $qr->save();
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()]);
        }
        return $this->index();
    }
}
