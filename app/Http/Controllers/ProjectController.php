<?php

namespace App\Http\Controllers;

use App\Helpers\ProjectHelpers;
use App\Helpers\UserHelpers;
use App\Models\Project;
use App\Models\ProjectAnswer;
use App\Models\ProjectQuest;
use App\Models\QuestRasp;
use App\Models\RefType;
use App\Models\User;
use App\Models\UsersTest;
use App\Models\UserTestAnswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class ProjectController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('id')){
            setcookie('test_id', $request->id);
        }

        if(UserHelpers::isAdmin()) {
            $q = Project::all();
            $type = RefType::all();
            return view('project.index', ["data" => $q, 'type' => $type]);
        }else{
            return $this->begin($request);
        }
    }

    public function begin(Request $request)
    {
        if($request->has('id')) {
            setcookie('test_id', $request->id);
            $q = ProjectHelpers::startProjectFromUid($request->id);
            if (!$q) {
                return view('noproject');
            }
        }else{
            try {
                $q = Project::find(ProjectHelpers::getProjectId());
            }catch (\Exception $e){
                return view('noproject');
            }
        }

        $type = $q->refType->name;
        try {
            $project = ProjectHelpers::getUserProject();
        }catch (\Exception $e){
            return Redirect::refresh();
        }
        if(!$project) {
            return view('startproject', ['type' => $type]);
        }



        $usertest = UsersTest::query()
            ->where([
                'id_project' => $q->id,
                "id_user" => Auth::id()
            ])
            ->first();
        $user_res = $usertest->userTestAnswers;

        if(count($user_res) > 0){
            return $this->projectResult($q->id, Auth::id());
        }

        $quests = ProjectHelpers::getProjectQuest($q->id, (count($user_res) == 0));

        $view = (count($user_res) > 0) ? 'test.testing_result' : 'test.testing';
        if($q->refType->id == 2){
            $view = (count($user_res) > 0) ? 'test.questing_result' : 'test.questing';
        }

        $ur = [];
        if(count($user_res) > 0){
            if($q->refType->id == 2) {
                $ur = $user_res;
            }else {
                foreach ($user_res as $user_re) {
                    $ur[$user_re->id_answer] = '';
                }
            }
        }

        return view($view, [
            "user" => UserHelpers::getUserDan(),
            "id_test" => $project->id,
            "project" => $project->project,
            "type" => $type,
            "quest" => $quests,
            "user_result" => $ur
        ]);

    }

    public function projectResult($id_project, $id_user)
    {
        $q = Project::find($id_project);
        $type = $q->refType->name;

        $project = UsersTest::query()
            ->where(['id_user' => $id_user, "id_project" => $id_project])
            ->first();

        $result = ProjectHelpers::getProjectQuestOnResult($id_project, $id_user);

        $result_true = 0;
        $result_false = 0;

        foreach($result as $res){
            if($res->correct == 1){
                $result_true++;
            }else{
                $result_false++;
            }
        }
        $ball = round($result_true / count($result)  * 100, 2);

        $data = [
            "user" => User::find($id_user),
            "id_test" => $project->id,
            "project" => $project->project,
            "type" => $type,
            "result" => $result,
            'res_true' => $result_true,
            'res_false' => $result_false,
            'ball' => $ball
        ];
        return view('test.result', $data);
    }

    public function start(Request $request)
    {
        if($request->method() == 'POST'){
            ProjectHelpers::SetUserProject();
        }
        //$this->begin($request);
        return Redirect::refresh();
    }

    private function getProjectDan($id)
    {
        return ProjectQuest::with('projectAnswers')
            ->leftJoin('quest_rasp', 'project_quest.id_rasp', '=', 'quest_rasp.id')
            ->where('id_project', $id)
            ->get([
                'project_quest.id',
                'project_quest.name',
                'project_quest.id_rasp',
                'project_quest.id_type',
                'quest_rasp.name as rasp_name'
            ]);
    }

    public function show($id)
    {
        $project = Project::find($id);
        $rasp = QuestRasp::all();
        $quest = $this->getProjectDan($id);
        return view('project.show', ['data' => $project, "rasp" => $rasp, "quests" => $quest]);
    }

    public function edit($id)
    {
        return response()->json(Project::find($id));
    }

    public function store(Request $request)
    {
        $id = ($request->has('id'))? $request->post('id') : 0;
        $name = $request->post('name');
        $id_type = $request->post('id_type');
        $date_begin = $request->post('date_begin');
        $date_end = $request->post('date_end');
        $cnt_quest = $request->post('cnt_quest');

        $project = ($id == 0) ? new Project() : Project::find($id);
        $project->id_user = \auth()->id();
        $project->id_type = $id_type;
        $project->name = $name;
        $project->date_begin = date("Y-m-d", strtotime($date_begin));
        $project->cnt_quest = $cnt_quest;


        if($date_end !== null) {
            $project->date_end = date("Y-m-d", strtotime($date_end));
        }

        if($id == 0){
            $project->uid = uniqid().uniqid();
        }

        $project->saveOrFail();
        return redirect()->to('/');
    }

    public function save_quest(Request $request, $id_project)
    {
        $id = ($request->has('id')) ? $request->post('id') : 0;
        $id_rasp = $request->post('id_rasp');
        $name = $request->post('quest_name');
        $id_true = $request->post('answer_true');
        $answers = $request->post('answer');

        if((int) $id > 0 ){
            $q = ProjectQuest::find($id);
            ProjectAnswer::query()->where('id_quest', $q->id)->delete();
            $q->delete();
        }

        $pq = new ProjectQuest();
        if($id > 0){
            $pq->id = $id;
        }
        $pq->id_project = $id_project;
        $pq->name = $name;
        $pq->id_rasp = $id_rasp;
        $pq->id_type = ($request->has('id_type')) ? 1 : 0;
        try {
            $pq->save();
        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()]);
        }

        $id_quest = $pq->id;

        //Если опросник
        if($request->has('id_type')){
            $pa = new ProjectAnswer();
            $pa->id_quest = $id_quest;
            $pa->name = $name;
            $pa->correct = false;
            try {
                $pa->save();
            } catch (\Exception $e) {
                return response()->json(['message' => $e->getMessage()]);
            }
        }else {
            //Если тестирование
            foreach ($answers as $k => $answer) {
                $pa = new ProjectAnswer();
                $pa->id_quest = $id_quest;
                $pa->name = $answer;
                $pa->correct = ($k == $id_true);
                try {
                    $pa->save();
                } catch (\Exception $e) {
                    return response()->json(['message' => $e->getMessage()]);
                }
            }
        }

        $pd = $this->getProjectDan($id_project);
        return response()->json(['data' => $pd]);
    }
}
