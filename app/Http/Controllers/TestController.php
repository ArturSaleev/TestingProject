<?php

namespace App\Http\Controllers;

use App\Models\UserTestAnswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
    public function store(Request $request)
    {
        $id_test = $request->post('id_test');
        $answers = $request->post('answer');
        foreach($answers as $k=>$v){
            $ua = new UserTestAnswer();
            $ua->id_user_test = $id_test;
            $ua->id_answer = $v;
            $ua->save();
        }

        if($request->has('answer_text')){
            $answer_text = $request->post('answer_text');
            foreach($answer_text as $k=>$v){
                $ua = new UserTestAnswer();
                $ua->id_user_test = $id_test;
                $ua->id_answer = $k;
                $ua->result_text = $v;
                $ua->save();
            }
        }
        return redirect()->to('/');
    }
}
