<?php


namespace App\Helpers;


use App\Models\Project;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserHelpers
{
    public static function isAdmin()
    {
        $id = Auth::id();
        $q = User::find($id);
        return (boolean) $q->isadmin;
    }

    public static function getUserDan()
    {
        $id = Auth::id();
        return User::find($id);
    }
}
