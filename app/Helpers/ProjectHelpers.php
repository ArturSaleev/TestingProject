<?php


namespace App\Helpers;


use App\Models\Project;
use App\Models\ProjectAnswer;
use App\Models\ProjectQuest;
use App\Models\UsersTest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;

class ProjectHelpers
{
    public static function getProjectId()
    {
        $uid = $_COOKIE['test_id'];
        return Project::query()->where('uid', $uid)->first()->id;
    }

    public static function startProjectFromUid($uid)
    {
        return Project::query()
            ->where('uid', $uid)
            ->whereNull('date_end')
            ->orWhere('date_end', '>', date('Y-m-d'))
            ->first();
    }

    public static function getUserProject()
    {
        $id_project = self::getProjectId();
        $id_user = Auth::id();
        $q = UsersTest::query()
            ->where(['id_user' => $id_user, "id_project" => $id_project])
            ->first();
        return $q;
    }

    public static function SetUserProject()
    {
        $id_project = self::getProjectId();
        $id_user = Auth::id();
        $q = self::getUserProject();
        if(!$q) {
            $userTest = new UsersTest();
            $userTest->id_user = $id_user;
            $userTest->id_project = $id_project;
            $userTest->date_begin = date('Y-m-d H:i:s');
            $userTest->save();
            $id = $userTest->id;
        }else{
            $id = $q->id;
        }
        return $id;
    }

    public static function getProjectQuest($id_project, $onRand = true)
    {
        $pr = Project::find($id_project);

        DB::enableQueryLog();
        $rand_text = ",dbms_random.value(1,1000) as tst";
        $connect = ENV('DB_CONNECTION');

        $cols = [
            'project_quest.id',
            'project_quest.name',
            'project_quest.id_type',
            'quest_rasp.name as rasp_name'
        ];

        $res = ProjectQuest::query()
            ->leftJoin('quest_rasp', 'project_quest.id_rasp', '=', 'quest_rasp.id')
            ->with('projectAnswers')
            ->where('id_project', $id_project);
            //->orderBy('project_quest.id_rasp')
            //->select($cols);

//        if((int) $pr->cnt_quest > 0){
            if($connect == 'oracle'){
                $cols = [
                    'project_quest.id',
                    'project_quest.name',
                    'project_quest.id_type',
                    'quest_rasp.name as rasp_name',
                    ($onRand) ? DB::raw('dbms_random.value(1,1000) as tst') : DB::raw('project_quest.id as tst')
                ];

                $res->orderBy('tst')->select($cols);
            }else{
                $res->inRandomOrder()
                    ->orderBy('project_quest.id_rasp')
                    ->select($cols);
            }
            //->limit($pr->cnt_quest);
            $res->limit($pr->cnt_quest);
//        }

        $result = $res->get();
        //dd(DB::getQueryLog());

        return $result;
    }

    public static function getProjectQuestOnResult($id_project, $id_user)
    {
        $sql = "select pq.id, pq.NAME, pa.ID as id_answer, pa.NAME as name_answer, pa.CORRECT, qr.NAME as rasp_name
                from PROJECT_QUEST pq,
                     PROJECT_ANSWERS pa,
                     QUEST_RASP qr
                where pa.ID_QUEST = pq.ID
                  and qr.ID = pq.ID_RASP
                  and pq.ID_PROJECT = $id_project
                  and pa.id in(
                      select uta.ID_ANSWER
                      from USER_TEST_ANSWERS uta,
                           USERS_TEST ut
                      where uta.ID_USER_TEST = ut.ID
                      and ut.ID_PROJECT = $id_project
                      and ut.ID_USER = $id_user
                    )
                order by pq.id, pa.ID";

        $q = DB::select($sql);
        foreach($q as $k=>$v){
            $q[$k]->list_answer = ProjectAnswer::query()->where('id_quest', $v->id)->get(['id', 'name']);
        }
        return $q;
    }

    public static function getResultAnswerId($id_project, $id_user, $id_answer)
    {
        $q = DB::selectOne("
        select
               uta.ID_ANSWER,
               uta.RESULT_TEXT
        from
             USER_TEST_ANSWERS uta,
             USERS_TEST ut,
             PROJECT_ANSWERS pa
        where
              uta.ID_USER_TEST = ut.id
          and pa.ID = uta.ID_ANSWER
          and ut.ID_PROJECT = $id_project
          and ut.ID_USER = $id_user
          and uta.ID_ANSWER = $id_answer
        ");

        if($q){
            return $q->result_text;
        }
        return '';
    }
}
